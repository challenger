/*
     This file is part of Challenger.
     Copyright (C) 2023 Taler Systems SA

     Challenger is free software; you can redistribute it and/or modify
     it under the terms of the GNU General Public License as published
     by the Free Software Foundation; either version 3, or (at your
     option) any later version.

     Challenger is distributed in the hope that it will be useful, but
     WITHOUT ANY WARRANTY; without even the implied warranty of
     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
     General Public License for more details.

     You should have received a copy of the GNU General Public License
     along with Challenger; see the file COPYING.  If not, write to the
     Free Software Foundation, Inc., 51 Franklin Street, Fifth Floor,
     Boston, MA 02110-1301, USA.
*/

/**
 * @file os_installation.c
 * @brief initialize libgnunet OS subsystem for Challenger.
 * @author Christian Grothoff
 */
#include "platform.h"
#include <gnunet/gnunet_util_lib.h>
#include "challenger_util.h"

/**
 * Default project data used for installation path detection
 * for GNU Challenger.
 */
static const struct GNUNET_OS_ProjectData challenger_pd = {
  .libname = "libchallengerutil",
  .project_dirname = "challenger",
  .binary_name = "challenger-httpd",
  .env_varname = "CHALLENGER_PREFIX",
  .base_config_varname = "CHALLENGER_BASE_CONFIG",
  .bug_email = "taler@lists.gnu.org",
  .homepage = "http://www.gnu.org/s/taler/",
  .config_file = "challenger.conf",
  .user_config_file = "~/.config/challenger.conf",
  .version = PACKAGE_VERSION,
  .is_gnu = 1,
  .gettext_domain = "challenger",
  .gettext_path = NULL,
};


const struct GNUNET_OS_ProjectData *
CHALLENGER_project_data (void)
{
  return &challenger_pd;
}


/* end of os_installation.c */
