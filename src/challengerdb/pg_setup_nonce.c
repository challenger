/*
   This file is part of Challenger
   Copyright (C) 2023 Taler Systems SA

   Challenger is free software; you can redistribute it and/or modify it under the
   terms of the GNU General Public License as published by the Free Software
   Foundation; either version 3, or (at your option) any later version.

   Challenger is distributed in the hope that it will be useful, but WITHOUT ANY
   WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR
   A PARTICULAR PURPOSE.  See the GNU General Public License for more details.

   You should have received a copy of the GNU General Public License along with
   Challenger; see the file COPYING.  If not, see <http://www.gnu.org/licenses/>
 */
/**
 * @file challengerdb/pg_setup_nonce.c
 * @brief Implementation of the validation_setup function for Postgres
 * @author Christian Grothoff
 */
#include "platform.h"
#include <taler/taler_error_codes.h>
#include <taler/taler_dbevents.h>
#include <taler/taler_pq_lib.h>
#include "pg_setup_nonce.h"
#include "pg_helper.h"


enum GNUNET_DB_QueryStatus
CH_PG_setup_nonce (void *cls,
                   uint64_t client_id,
                   const struct CHALLENGER_ValidationNonceP *nonce,
                   struct GNUNET_TIME_Absolute expiration_time,
                   const json_t *initial_address)
{
  struct PostgresClosure *pg = cls;
  struct GNUNET_PQ_QueryParam params[] = {
    GNUNET_PQ_query_param_uint64 (&client_id),
    GNUNET_PQ_query_param_auto_from_type (nonce),
    GNUNET_PQ_query_param_absolute_time (&expiration_time),
    NULL == initial_address
    ? GNUNET_PQ_query_param_null ()
    : TALER_PQ_query_param_json (initial_address),
    GNUNET_PQ_query_param_end
  };

  PREPARE (pg,
           "setup_nonce",
           "INSERT INTO validations"
           " (client_serial_id"
           " ,nonce"
           " ,expiration_time"
           " ,client_redirect_uri"
           " ,address"
           ") SELECT $1, $2, $3, uri, $4"
           " FROM CLIENTS"
           " WHERE client_serial_id=$1;");
  return GNUNET_PQ_eval_prepared_non_select (pg->conn,
                                             "setup_nonce",
                                             params);
}
