/*
   This file is part of Challenger
   Copyright (C) 2023 Taler Systems SA

   Challenger is free software; you can redistribute it and/or modify it under the
   terms of the GNU General Public License as published by the Free Software
   Foundation; either version 3, or (at your option) any later version.

   Challenger is distributed in the hope that it will be useful, but WITHOUT ANY
   WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR
   A PARTICULAR PURPOSE.  See the GNU General Public License for more details.

   You should have received a copy of the GNU General Public License along with
   Challenger; see the file COPYING.  If not, see <http://www.gnu.org/licenses/>
 */
/**
 * @file challengerdb/pg_validate_login_address.c
 * @brief Implementation of the validate_login_address function for Postgres
 * @author Christian Grothoff
 */
#include "platform.h"
#include <taler/taler_error_codes.h>
#include <taler/taler_dbevents.h>
#include <taler/taler_pq_lib.h>
#include "pg_validate_login_address.h"
#include "pg_helper.h"


enum GNUNET_DB_QueryStatus
CH_PG_validate_login_address (void *cls,
                              const struct CHALLENGER_ValidationNonceP *nonce,
                              const char *address,
                              const char *client_scope,
                              const char *client_state,
                              const char *client_redirect_uri,
                              struct GNUNET_TIME_Absolute *last_tx_time,
                              uint32_t *last_pin,
                              uint32_t *pin_attempts_left)
{
  struct PostgresClosure *pg = cls;
  struct GNUNET_PQ_QueryParam params[] = {
    GNUNET_PQ_query_param_auto_from_type (nonce),
    GNUNET_PQ_query_param_string (address),
    GNUNET_PQ_query_param_string (client_scope),
    GNUNET_PQ_query_param_string (client_state),
    NULL != client_redirect_uri
    ? GNUNET_PQ_query_param_string (client_redirect_uri)
    : GNUNET_PQ_query_param_null (),
    GNUNET_PQ_query_param_end
  };
  struct GNUNET_PQ_ResultSpec rs[] = {
    GNUNET_PQ_result_spec_absolute_time ("last_tx_time",
                                         last_tx_time),
    GNUNET_PQ_result_spec_uint32 ("last_pin",
                                  last_pin),
    GNUNET_PQ_result_spec_uint32 ("pin_attempts_left",
                                  pin_attempts_left),
    GNUNET_PQ_result_spec_end
  };

  PREPARE (pg,
           "validate_set_address",
           "UPDATE validations SET"
           "  address_attempts_left=CASE"
           "    WHEN address != $2"
           "    THEN address_attempts_left - 1"
           "    ELSE address_attempts_left"
           "  END"
           " ,address=$2"
           " ,client_scope=$3"
           " ,client_state=$4"
           " ,client_redirect_uri=$5"
           " WHERE nonce=$1"
           "   AND (address_attempts_left > 0"
           "        OR address == $2)"
           " RETURNING"
           "   last_tx_time"
           "  ,last_pin"
           "  ,pin_attempts_left;");
  return GNUNET_PQ_eval_prepared_singleton_select (pg->conn,
                                                   "validate_set_address",
                                                   params,
                                                   rs);
}
