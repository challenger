/*
   This file is part of Challenger
   Copyright (C) 2023 Taler Systems SA

   Challenger is free software; you can redistribute it and/or modify it under the
   terms of the GNU General Public License as published by the Free Software
   Foundation; either version 3, or (at your option) any later version.

   Challenger is distributed in the hope that it will be useful, but WITHOUT ANY
   WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR
   A PARTICULAR PURPOSE.  See the GNU General Public License for more details.

   You should have received a copy of the GNU General Public License along with
   Challenger; see the file COPYING.  If not, see <http://www.gnu.org/licenses/>
 */
/**
 * @file challengerdb/pg_setup_nonce.h
 * @brief implementation of the validation_setup function for Postgres
 * @author Christian Grothoff
 */
#ifndef PG_VALIDATION_SETUP_H
#define PG_VALIDATION_SETUP_H

#include <taler/taler_util.h>
#include <taler/taler_json_lib.h>
#include "challenger_database_plugin.h"

/**
 * Start validation process by setting up a validation entry. Allows
 * the respective user who learns the @a nonce to later begin the
 * process.
 *
 * @param cls closure
 * @param client_id ID of the client
 * @param nonce unique nonce to use to identify the validation
 * @param expiration_time when will the validation expire
 * @param initial_address address the user should validate,
 *        NULL if the user should enter it themselves
 * @return transaction status
 */
enum GNUNET_DB_QueryStatus
CH_PG_setup_nonce (void *cls,
                   uint64_t client_id,
                   const struct CHALLENGER_ValidationNonceP *nonce,
                   struct GNUNET_TIME_Absolute expiration_time,
                   const json_t *initial_address);

#endif
