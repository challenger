/*
   This file is part of Challenger
   Copyright (C) 2023 Taler Systems SA

   Challenger is free software; you can redistribute it and/or modify it under the
   terms of the GNU General Public License as published by the Free Software
   Foundation; either version 3, or (at your option) any later version.

   Challenger is distributed in the hope that it will be useful, but WITHOUT ANY
   WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR
   A PARTICULAR PURPOSE.  See the GNU General Public License for more details.

   You should have received a copy of the GNU General Public License along with
   Challenger; see the file COPYING.  If not, see <http://www.gnu.org/licenses/>
 */
/**
 * @file challengerdb/pg_authorize_start.h
 * @brief implementation of the authorize_start function for Postgres
 * @author Christian Grothoff
 * @author Bohdan Potuzhnyi
 * @author Vlada Svirsh
 */
#ifndef PG_LOGIN_START_H
#define PG_LOGIN_START_H

#include <taler/taler_util.h>
#include <taler/taler_json_lib.h>
#include "challenger_database_plugin.h"


/**
 * Set the user-provided address in a validation process. Updates
 * the address and decrements the "addresses left" counter.  If the
 * address did not change, the operation is successful even without
 * the counter change.
 *
 * @param cls
 * @param nonce unique nonce to use to identify the validation
 * @param client_id client that initiated the validation
 * @param client_scope scope of the validation
 * @param client_state state of the client
 * @param client_redirect_uri where to redirect at the end, NULL to use a unique one registered for the client
 * @param code_challenge PKCE code challenge
 * @param code_challenge_method PKCE code challenge method enum
 * @param[out] last_address set to the last address used
 * @param[out] address_attempts_left set to number of address changing attempts left for this address
 * @param[out] pin_transmissions_left set to number of times the PIN can still be re-requested
 * @param[out] auth_attempts_left set to number of authentication attempts remaining
 * @param[out] solved set to true if the challenge is already solved
 * @param[out] last_tx_time set to the last time when we (presumably) send a PIN to @a last_address; 0 if never sent
 * @return transaction status:
 *   #GNUNET_DB_STATUS_SUCCESS_ONE_RESULT if the address was changed
 *   #GNUNET_DB_STATUS_SUCCESS_NO_RESULTS if we do not permit further changes to the address (attempts exhausted)
 *   #GNUNET_DB_STATUS_HARD_ERROR on failure
 */
enum GNUNET_DB_QueryStatus
CH_PG_authorize_start (void *cls,
                       const struct CHALLENGER_ValidationNonceP *nonce,
                       uint64_t client_id,
                       const char *client_scope,
                       const char *client_state,
                       const char *client_redirect_uri,
                       const char *code_challenge,
                       uint32_t code_challenge_method,
                       json_t **last_address,
                       uint32_t *address_attempts_left,
                       uint32_t *pin_transmissions_left,
                       uint32_t *auth_attempts_left,
                       bool *solved,
                       struct GNUNET_TIME_Absolute *last_tx_time);


#endif
