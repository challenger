/*
   This file is part of Challenger
   Copyright (C) 2023 Taler Systems SA

   Challenger is free software; you can redistribute it and/or modify it under the
   terms of the GNU General Public License as published by the Free Software
   Foundation; either version 3, or (at your option) any later version.

   Challenger is distributed in the hope that it will be useful, but WITHOUT ANY
   WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR
   A PARTICULAR PURPOSE.  See the GNU General Public License for more details.

   You should have received a copy of the GNU General Public License along with
   Challenger; see the file COPYING.  If not, see <http://www.gnu.org/licenses/>
 */
/**
 * @file challengerdb/pg_validate_login_pin.c
 * @brief Implementation of the validate_login_pin function for Postgres
 * @author Christian Grothoff
 */
#include "platform.h"
#include <taler/taler_error_codes.h>
#include <taler/taler_dbevents.h>
#include <taler/taler_pq_lib.h>
#include "pg_validate_login_pin.h"
#include "pg_helper.h"


enum GNUNET_DB_QueryStatus
CH_PG_validate_login_pin (void *cls,
                          const struct CHALLENGER_ValidationNonceP *nonce,
                          struct GNUNET_TIME_Absolute tx_time,
                          uint32_t new_pin,
                          uint32_t auth_attempts_allowed)
{
  struct PostgresClosure *pg = cls;
  struct GNUNET_PQ_QueryParam params[] = {
    GNUNET_PQ_query_param_auto_from_type (nonce),
    GNUNET_PQ_query_param_absolute_time (&tx_time),
    GNUNET_PQ_query_param_uint32 (&new_pin),
    GNUNET_PQ_query_param_uint32 (&auth_attempts_allowed),
    GNUNET_PQ_query_param_end
  };

  PREPARE (pg,
           "validate_login_set_pin",
           "UPDATE validations SET"
           "  last_tx_time=$2"
           " ,last_pin=$3"
           " ,auth_attempts_left=$4"
           " ,pin_attempts_left=pin_attempts_left - 1"
           " WHERE nonce=$1"
           "   AND pin_attempts_left > 0;");
  return GNUNET_PQ_eval_prepared_non_select (pg->conn,
                                             "validate_login_set_pin",
                                             params);
}
