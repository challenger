/*
   This file is part of Challenger
   Copyright (C) 2023 Taler Systems SA

   Challenger is free software; you can redistribute it and/or modify it under the
   terms of the GNU General Public License as published by the Free Software
   Foundation; either version 3, or (at your option) any later version.

   Challenger is distributed in the hope that it will be useful, but WITHOUT ANY
   WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR
   A PARTICULAR PURPOSE.  See the GNU General Public License for more details.

   You should have received a copy of the GNU General Public License along with
   Challenger; see the file COPYING.  If not, see <http://www.gnu.org/licenses/>
 */
/**
 * @file challengerdb/pg_validate_login_address.h
 * @brief implementation of the validate_login_address function for Postgres
 * @author Christian Grothoff
 */
#ifndef PG_VALIDATE_LOGIN_ADDRESS_H
#define PG_VALIDATE_LOGIN_ADDRESS_H

#include <taler/taler_util.h>
#include <taler/taler_json_lib.h>
#include "challenger_database_plugin.h"


/**
 * Set the user-provided address in a validation process. Updates
 * the address and decrements the "addresses left" counter.  If the
 * address did not change, the operation is successful even without
 * the counter change.
 *
 * @param cls
 * @param nonce unique nonce to use to identify the validation
 * @param address the new address to validate
 * @param client_scope scope of the validation
 * @param client_state state of the client
 * @param client_redirect_uri where to redirect at the end, NULL to use a unique one registered for the client
 * @param[out] last_tx_time set to the last time when we (presumably) send a PIN to @a address; 0 if never sent
 * @param[out] last_pin set to the PIN last send to @a address, 0 if never sent
 * @param[in,out] pin_attempts_left set to number of PIN transmission attempts left for this address; input is value to be used if address is new, output is possibly different if address was not new
 * @return transaction status:
 *   #GNUNET_DB_STATUS_SUCCESS_ONE_RESULT if the address was changed
 *   #GNUNET_DB_STATUS_SUCCESS_NO_RESULTS if we do not permit further changes to the address (attempts exhausted)
 *   #GNUNET_DB_STATUS_HARD_ERROR on failure
 */
enum GNUNET_DB_QueryStatus
CH_PG_validate_login_address (void *cls,
                              const struct CHALLENGER_ValidationNonceP *nonce,
                              const char *address,
                              const char *client_scope,
                              const char *client_state,
                              const char *client_redirect_uri,
                              struct GNUNET_TIME_Absolute *last_tx_time,
                              uint32_t *last_pin,
                              uint32_t *pin_attempts_left);


#endif
