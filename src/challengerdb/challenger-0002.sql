--
-- This file is part of Challenger
-- Copyright (C) 2023 Taler Systems SA
--
-- Challenger is free software; you can redistribute it and/or modify it under the
-- terms of the GNU General Public License as published by the Free Software
-- Foundation; either version 3, or (at your option) any later version.
--
-- Challenger is distributed in the hope that it will be useful, but WITHOUT ANY
-- WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR
-- A PARTICULAR PURPOSE.  See the GNU General Public License for more details.
--
-- You should have received a copy of the GNU General Public License along with
-- Challenger; see the file COPYING.  If not, see <http://www.gnu.org/licenses/>
--

-- Everything in one big transaction
BEGIN;

-- Check patch versioning is in place.
SELECT _v.register_patch('challenger-0002', NULL, NULL);

SET search_path TO challenger;


-- Add columns for PKCE (RFC 7636)
ALTER TABLE validations
  ADD COLUMN IF NOT EXISTS code_challenge VARCHAR,
  ADD COLUMN IF NOT EXISTS code_challenge_method INT4 DEFAULT(0);

COMMENT ON COLUMN validations.code_challenge
  IS 'Code challenge used for PKCE';
COMMENT ON COLUMN validations.code_challenge_method
  IS 'Code challenge method used for PKCE (plain, s256)';

COMMIT;
