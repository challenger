/*
   This file is part of Challenger
   Copyright (C) 2023 Taler Systems SA

   Challenger is free software; you can redistribute it and/or modify it under the
   terms of the GNU General Public License as published by the Free Software
   Foundation; either version 3, or (at your option) any later version.

   Challenger is distributed in the hope that it will be useful, but WITHOUT ANY
   WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR
   A PARTICULAR PURPOSE.  See the GNU General Public License for more details.

   You should have received a copy of the GNU General Public License along with
   Challenger; see the file COPYING.  If not, see <http://www.gnu.org/licenses/>
 */
/**
 * @file challengerdb/pg_client_delete.h
 * @brief implementation of the client_delete function for Postgres
 * @author Christian Grothoff
 */
#ifndef PG_CLIENT_DELETE_H
#define PG_CLIENT_DELETE_H

#include <taler/taler_util.h>
#include <taler/taler_json_lib.h>
#include "challenger_database_plugin.h"

/**
 * Delete client from the list of authorized clients.
 *
 * @param cls
 * @param client_url URL of the client
 * @return transaction status
 */
enum GNUNET_DB_QueryStatus
CH_PG_client_delete (void *cls,
                     const char *client_url);

#endif
