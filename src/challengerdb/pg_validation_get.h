/*
   This file is part of Challenger
   Copyright (C) 2023 Taler Systems SA

   Challenger is free software; you can redistribute it and/or modify it under the
   terms of the GNU General Public License as published by the Free Software
   Foundation; either version 3, or (at your option) any later version.

   Challenger is distributed in the hope that it will be useful, but WITHOUT ANY
   WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR
   A PARTICULAR PURPOSE.  See the GNU General Public License for more details.

   You should have received a copy of the GNU General Public License along with
   Challenger; see the file COPYING.  If not, see <http://www.gnu.org/licenses/>
 */
/**
 * @file challengerdb/pg_validation_get.h
 * @brief implementation of the validation_get function for Postgres
 * @author Christian Grothoff
 */
#ifndef PG_VALIDATION_GET_H
#define PG_VALIDATION_GET_H

#include <taler/taler_util.h>
#include <taler/taler_json_lib.h>
#include "challenger_database_plugin.h"


/**
 * Return validation details.  Used by ``/solve``, ``/auth`` and
 * ``/info`` endpoints to authorize and return validated user
 * address to the client.
 *
 * @param cls
 * @param nonce unique nonce to use to identify the validation
 * @param[out] client_secret set to secret of client (for client that setup the challenge)
 * @param[out] address set to client-provided address
 * @param[out] client_scope set to OAuth2 scope
 * @param[out] client_state set to client state
 * @param[out] client_redirect_uri set to client redirect URL
 * @return transaction status:
 *   #GNUNET_DB_STATUS_SUCCESS_ONE_RESULT if the nonce was found
 *   #GNUNET_DB_STATUS_SUCCESS_NO_RESULTS if we do not know the nonce
 *   #GNUNET_DB_STATUS_HARD_ERROR on failure
 */
enum GNUNET_DB_QueryStatus
CH_PG_validation_get (void *cls,
                      const struct CHALLENGER_ValidationNonceP *nonce,
                      char **client_secret,
                      json_t **address,
                      char **client_scope,
                      char **client_state,
                      char **client_redirect_uri);

#endif
