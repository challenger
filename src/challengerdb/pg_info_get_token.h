/*
   This file is part of Challenger
   Copyright (C) 2023 Taler Systems SA

   Challenger is free software; you can redistribute it and/or modify it under the
   terms of the GNU General Public License as published by the Free Software
   Foundation; either version 3, or (at your option) any later version.

   Challenger is distributed in the hope that it will be useful, but WITHOUT ANY
   WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR
   A PARTICULAR PURPOSE.  See the GNU General Public License for more details.

   You should have received a copy of the GNU General Public License along with
   Challenger; see the file COPYING.  If not, see <http://www.gnu.org/licenses/>
 */
/**
 * @file challengerdb/pg_info_get_token.h
 * @brief implementation of the info_get_token function for Postgres
 * @author Christian Grothoff
 */
#ifndef PG_INFO_GET_GRANT_H
#define PG_INFO_GET_GRANT_H

#include <taler/taler_util.h>
#include <taler/taler_json_lib.h>
#include "challenger_database_plugin.h"


/**
 * Return @a address which @a grant gives access to.
 *
 * @param cls closure
 * @param grant grant token that grants access
 * @param[out] rowid account identifier within challenger
 * @param[out] address set to the address under @a grant
 * @param[out] address_expiration set to how long we consider @a address to be valid
 * @return transaction status
 */
enum GNUNET_DB_QueryStatus
CH_PG_info_get_token (
  void *cls,
  const struct CHALLENGER_AccessTokenP *grant,
  uint64_t *rowid,
  json_t **address,
  struct GNUNET_TIME_Timestamp *address_expiration);


#endif
