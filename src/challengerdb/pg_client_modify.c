/*
   This file is part of Challenger
   Copyright (C) 2024 Taler Systems SA

   Challenger is free software; you can redistribute it and/or modify it under the
   terms of the GNU General Public License as published by the Free Software
   Foundation; either version 3, or (at your option) any later version.

   Challenger is distributed in the hope that it will be useful, but WITHOUT ANY
   WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR
   A PARTICULAR PURPOSE.  See the GNU General Public License for more details.

   You should have received a copy of the GNU General Public License along with
   Challenger; see the file COPYING.  If not, see <http://www.gnu.org/licenses/>
 */
/**
 * @file challengerdb/pg_client_modify.c
 * @brief Implementation of the client_modify function for Postgres
 * @author Christian Grothoff
 */
#include "platform.h"
#include <taler/taler_error_codes.h>
#include <taler/taler_dbevents.h>
#include <taler/taler_pq_lib.h>
#include "pg_client_modify.h"
#include "pg_helper.h"

enum GNUNET_DB_QueryStatus
CH_PG_client_modify (void *cls,
                     uint64_t client_id,
                     const char *client_redirect_uri,
                     const char *client_secret)
{
  struct PostgresClosure *pg = cls;
  struct GNUNET_PQ_QueryParam params[] = {
    GNUNET_PQ_query_param_uint64 (&client_id),
    GNUNET_PQ_query_param_string (client_redirect_uri),
    NULL == client_secret
    ? GNUNET_PQ_query_param_null ()
    : GNUNET_PQ_query_param_string (client_secret),
    GNUNET_PQ_query_param_end
  };

  PREPARE (pg,
           "client_modify",
           "UPDATE clients"
           " SET uri=$2"
           "    ,client_secret=COALESCE($3,client_secret)"
           " WHERE client_serial_id=$1");
  return GNUNET_PQ_eval_prepared_non_select (pg->conn,
                                             "client_modify",
                                             params);
}
