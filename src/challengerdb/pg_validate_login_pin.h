/*
   This file is part of Challenger
   Copyright (C) 2023 Taler Systems SA

   Challenger is free software; you can redistribute it and/or modify it under the
   terms of the GNU General Public License as published by the Free Software
   Foundation; either version 3, or (at your option) any later version.

   Challenger is distributed in the hope that it will be useful, but WITHOUT ANY
   WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR
   A PARTICULAR PURPOSE.  See the GNU General Public License for more details.

   You should have received a copy of the GNU General Public License along with
   Challenger; see the file COPYING.  If not, see <http://www.gnu.org/licenses/>
 */
/**
 * @file challengerdb/pg_validate_login_pin.h
 * @brief implementation of the validate_login_pin function for Postgres
 * @author Christian Grothoff
 */
#ifndef PG_VALIDATE_LOGIN_PIN_H
#define PG_VALIDATE_LOGIN_PIN_H

#include <taler/taler_util.h>
#include <taler/taler_json_lib.h>
#include "challenger_database_plugin.h"


/**
 * Store a new PIN to be used to validate an address.
 *
 * @param cls
 * @param nonce unique nonce to use to identify the validation
 * @param tx_time the current time
 * @param new_pin the PIN we are sending
 * @param auth_attempts_allowed how many attempts do we give to the user to enter the correct PIN
 * @return transaction status:
 *   #GNUNET_DB_STATUS_SUCCESS_ONE_RESULT if the pin was stored
 *   #GNUNET_DB_STATUS_SUCCESS_NO_RESULTS if we do not know the @a nonce or if pin attempts left is zero
 *   #GNUNET_DB_STATUS_HARD_ERROR on failure
 */
enum GNUNET_DB_QueryStatus
CH_PG_validate_login_pin (void *cls,
                          const struct CHALLENGER_ValidationNonceP *nonce,
                          struct GNUNET_TIME_Absolute tx_time,
                          uint32_t new_pin,
                          uint32_t auth_attempts_allowed);


#endif
