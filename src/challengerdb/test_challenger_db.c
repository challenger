/*
  This file is part of
  (C) 2023 Taler Systems SA

  Challenger is free software; you can redistribute it and/or modify it under the
  terms of the GNU Lesser General Public License as published by the Free Software
  Foundation; either version 3, or (at your option) any later version.

  Challenger is distributed in the hope that it will be useful, but WITHOUT ANY
  WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR
  A PARTICULAR PURPOSE.  See the GNU General Public License for more details.

  You should have received a copy of the GNU General Public License along with
  Challenger; see the file COPYING.  If not, see <http://www.gnu.org/licenses/>
*/
/**
 * @file sync/test_sync_db.c
 * @brief testcase for sync postgres db plugin
 * @author Christian Grothoff
 */
#include "platform.h"
#include <gnunet/gnunet_util_lib.h>
#include <taler/taler_util.h>
#include "challenger_database_plugin.h"
#include "challenger_database_lib.h"
#include "challenger_util.h"


#define FAILIF(cond)                            \
        do {                                          \
          if (! (cond)) { break;}                       \
          GNUNET_break (0);                           \
          goto drop;                                     \
        } while (0)

#define RND_BLK(ptr)                                                    \
        GNUNET_CRYPTO_random_block (GNUNET_CRYPTO_QUALITY_WEAK, ptr, sizeof (* \
                                                                             ptr))

/**
 * Global return value for the test.  Initially -1, set to 0 upon
 * completion.   Other values indicate some kind of error.
 */
static int result;

/**
 * Handle to the plugin we are testing.
 */
static struct CHALLENGER_DatabasePlugin *plugin;


/**
 * Main function that will be run by the scheduler.
 *
 * @param cls closure with config
 */
static void
run (void *cls)
{
  struct GNUNET_CONFIGURATION_Handle *cfg = cls;

  if (NULL == (plugin = CHALLENGER_DB_plugin_load (cfg,
                                                   true)))
  {
    result = 77;
    return;
  }
  if (GNUNET_OK !=
      plugin->drop_tables (plugin->cls))
  {
    GNUNET_log (GNUNET_ERROR_TYPE_INFO,
                "Dropping tables failed\n");
  }
  if (GNUNET_OK !=
      plugin->create_tables (plugin->cls))
  {
    GNUNET_log (GNUNET_ERROR_TYPE_INFO,
                "Creating tables failed\n");
  }
  GNUNET_assert (GNUNET_OK ==
                 plugin->preflight (plugin->cls));
  {
    struct GNUNET_TIME_Absolute ts = GNUNET_TIME_absolute_get ();

    FAILIF (0 >
            plugin->gc (plugin->cls,
                        ts));
  }
  result = 0;
drop:
  GNUNET_break (GNUNET_OK ==
                plugin->drop_tables (plugin->cls));
  CHALLENGER_DB_plugin_unload (plugin);
  plugin = NULL;
}


int
main (int argc,
      char *const argv[])
{
  const char *plugin_name;
  char *config_filename;
  char *testname;
  struct GNUNET_CONFIGURATION_Handle *cfg;

  (void) argc;
  result = EXIT_FAILURE;
  if (NULL == (plugin_name = strrchr (argv[0], (int) '-')))
  {
    GNUNET_break (0);
    return EXIT_FAILURE;
  }
  GNUNET_log_setup (argv[0],
                    "DEBUG",
                    NULL);
  plugin_name++;
  (void) GNUNET_asprintf (&testname,
                          "%s",
                          plugin_name);
  (void) GNUNET_asprintf (&config_filename,
                          "test_challenger_db_%s.conf",
                          testname);
  cfg = GNUNET_CONFIGURATION_create (CHALLENGER_project_data ());
  if (GNUNET_OK !=
      GNUNET_CONFIGURATION_parse (cfg,
                                  config_filename))
  {
    GNUNET_break (0);
    GNUNET_free (config_filename);
    GNUNET_free (testname);
    return EXIT_NOTCONFIGURED;
  }
  GNUNET_SCHEDULER_run (&run, cfg);
  GNUNET_CONFIGURATION_destroy (cfg);
  GNUNET_free (config_filename);
  GNUNET_free (testname);
  return result;
}


/* end of test_challenger_db.c */
