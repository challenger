/*
   This file is part of Challenger
   Copyright (C) 2023 Taler Systems SA

   Challenger is free software; you can redistribute it and/or modify it under the
   terms of the GNU General Public License as published by the Free Software
   Foundation; either version 3, or (at your option) any later version.

   Challenger is distributed in the hope that it will be useful, but WITHOUT ANY
   WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR
   A PARTICULAR PURPOSE.  See the GNU General Public License for more details.

   You should have received a copy of the GNU General Public License along with
   Challenger; see the file COPYING.  If not, see <http://www.gnu.org/licenses/>
 */
/**
 * @file challengerdb/pg_authorize_start.c
 * @brief Implementation of the authorize_start function for Postgres
 * @author Christian Grothoff
 * @author Bohdan Potuzhnyi
 * @author Vlada Svirsh
 */
#include "platform.h"
#include <taler/taler_error_codes.h>
#include <taler/taler_dbevents.h>
#include <taler/taler_pq_lib.h>
#include "pg_authorize_start.h"
#include "pg_helper.h"


enum GNUNET_DB_QueryStatus
CH_PG_authorize_start (void *cls,
                       const struct CHALLENGER_ValidationNonceP *nonce,
                       uint64_t client_id,
                       const char *client_scope,
                       const char *client_state,
                       const char *client_redirect_uri,
                       const char *code_challenge,
                       uint32_t code_challenge_method,
                       json_t **last_address,
                       uint32_t *address_attempts_left,
                       uint32_t *pin_transmissions_left,
                       uint32_t *auth_attempts_left,
                       bool *solved,
                       struct GNUNET_TIME_Absolute *last_tx_time)
{
  struct PostgresClosure *pg = cls;
  struct GNUNET_PQ_QueryParam params[] = {
    GNUNET_PQ_query_param_auto_from_type (nonce),
    GNUNET_PQ_query_param_uint64 (&client_id),
    NULL != client_scope
    ? GNUNET_PQ_query_param_string (client_scope)
    : GNUNET_PQ_query_param_null (),
    GNUNET_PQ_query_param_string (client_state),
    NULL != client_redirect_uri
    ? GNUNET_PQ_query_param_string (client_redirect_uri)
    : GNUNET_PQ_query_param_null (),
    NULL != code_challenge
    ? GNUNET_PQ_query_param_string (code_challenge)
    : GNUNET_PQ_query_param_null (),
    GNUNET_PQ_query_param_uint32 (&code_challenge_method),
    GNUNET_PQ_query_param_end
  };
  struct GNUNET_PQ_ResultSpec rs[] = {
    GNUNET_PQ_result_spec_allow_null (
      TALER_PQ_result_spec_json ("address",
                                 last_address),
      NULL),
    GNUNET_PQ_result_spec_uint32 ("address_attempts_left",
                                  address_attempts_left),
    GNUNET_PQ_result_spec_uint32 ("pin_transmissions_left",
                                  pin_transmissions_left),
    GNUNET_PQ_result_spec_uint32 ("auth_attempts_left",
                                  auth_attempts_left),
    GNUNET_PQ_result_spec_bool ("solved",
                                solved),
    GNUNET_PQ_result_spec_absolute_time ("last_tx_time",
                                         last_tx_time),
    GNUNET_PQ_result_spec_end
  };

  *last_address = NULL;
  PREPARE (pg,
           "authorize_start_validation",
           "UPDATE validations SET"
           "  client_scope=$3"
           " ,client_state=$4"
           " ,client_redirect_uri=$5::VARCHAR"
           " ,code_challenge=$6"
           " ,code_challenge_method=$7"
           " WHERE nonce=$1"
           "   AND client_serial_id=$2"
           "   AND ($5::VARCHAR=COALESCE(client_redirect_uri,$5::VARCHAR))"
           " RETURNING"
           "   address"
           "  ,address_attempts_left"
           "  ,pin_transmissions_left"
           "  ,GREATEST(0, auth_attempts_left) AS auth_attempts_left"
           "  ,auth_attempts_left = -1 AS solved"
           "  ,last_tx_time;");
  return GNUNET_PQ_eval_prepared_singleton_select (pg->conn,
                                                   "authorize_start_validation",
                                                   params,
                                                   rs);
}