/*
   This file is part of Challenger
   Copyright (C) 2023 Taler Systems SA

   Challenger is free software; you can redistribute it and/or modify it under the
   terms of the GNU General Public License as published by the Free Software
   Foundation; either version 3, or (at your option) any later version.

   Challenger is distributed in the hope that it will be useful, but WITHOUT ANY
   WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR
   A PARTICULAR PURPOSE.  See the GNU General Public License for more details.

   You should have received a copy of the GNU General Public License along with
   Challenger; see the file COPYING.  If not, see <http://www.gnu.org/licenses/>
 */
/**
 * @file challengerdb/pg_challenge_set_address_and_pin.c
 * @brief Implementation of the challenge_set_address_and_pin function for Postgres
 * @author Christian Grothoff
 */
#include "platform.h"
#include <taler/taler_error_codes.h>
#include <taler/taler_dbevents.h>
#include <taler/taler_pq_lib.h>
#include "pg_challenge_set_address_and_pin.h"
#include "pg_helper.h"


enum GNUNET_DB_QueryStatus
CH_PG_challenge_set_address_and_pin (
  void *cls,
  const struct CHALLENGER_ValidationNonceP *nonce,
  const json_t *address,
  struct GNUNET_TIME_Relative validation_duration,
  uint32_t *tan,
  char **state,
  struct GNUNET_TIME_Absolute *last_tx_time,
  uint32_t *auth_attempts_left,
  bool *pin_transmit,
  char **client_redirect_uri,
  bool *address_refused,
  bool *solved)
{
  struct PostgresClosure *pg = cls;
  struct GNUNET_TIME_Absolute now
    = GNUNET_TIME_absolute_get ();
  struct GNUNET_TIME_Absolute next_tx_time
    = GNUNET_TIME_absolute_subtract (now,
                                     validation_duration);
  struct GNUNET_PQ_QueryParam params[] = {
    GNUNET_PQ_query_param_auto_from_type (nonce),
    TALER_PQ_query_param_json (address),
    GNUNET_PQ_query_param_absolute_time (&next_tx_time),
    GNUNET_PQ_query_param_absolute_time (&now),
    GNUNET_PQ_query_param_uint32 (tan),
    GNUNET_PQ_query_param_end
  };
  bool not_found;
  struct GNUNET_PQ_ResultSpec rs[] = {
    GNUNET_PQ_result_spec_bool ("not_found",
                                &not_found),
    GNUNET_PQ_result_spec_absolute_time ("last_tx_time",
                                         last_tx_time),
    GNUNET_PQ_result_spec_uint32 ("last_pin",
                                  tan),
    GNUNET_PQ_result_spec_bool ("pin_transmit",
                                pin_transmit),
    GNUNET_PQ_result_spec_uint32 ("auth_attempts_left",
                                  auth_attempts_left),
    GNUNET_PQ_result_spec_allow_null (
      GNUNET_PQ_result_spec_string ("client_redirect_uri",
                                    client_redirect_uri),
      NULL),
    GNUNET_PQ_result_spec_allow_null (
      GNUNET_PQ_result_spec_string ("state",
                                    state),
      NULL),
    GNUNET_PQ_result_spec_bool ("address_refused",
                                address_refused),
    GNUNET_PQ_result_spec_bool ("solved",
                                solved),
    GNUNET_PQ_result_spec_end
  };
  enum GNUNET_DB_QueryStatus qs;

  *client_redirect_uri = NULL;
  PREPARE (pg,
           "do_challenge_set_address_and_pin",
           "SELECT "
           " out_not_found AS not_found"
           ",out_last_tx_time AS last_tx_time"
           ",out_pin_transmit AS pin_transmit"
           ",out_last_pin AS last_pin"
           ",out_state AS state"
           ",out_auth_attempts_left AS auth_attempts_left"
           ",out_client_redirect_uri AS client_redirect_uri"
           ",out_address_refused AS address_refused"
           ",out_solved AS solved"
           " FROM challenger_do_challenge_set_address_and_pin"
           " ($1,$2,$3,$4,$5);");
  qs = GNUNET_PQ_eval_prepared_singleton_select (pg->conn,
                                                 "do_challenge_set_address_and_pin",
                                                 params,
                                                 rs);
  if (qs <= 0)
    return qs;
  if (not_found)
    return GNUNET_DB_STATUS_SUCCESS_NO_RESULTS;
  return qs;
}
