/*
   This file is part of Challenger
   Copyright (C) 2023 Taler Systems SA

   Challenger is free software; you can redistribute it and/or modify it under the
   terms of the GNU General Public License as published by the Free Software
   Foundation; either version 3, or (at your option) any later version.

   Challenger is distributed in the hope that it will be useful, but WITHOUT ANY
   WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR
   A PARTICULAR PURPOSE.  See the GNU General Public License for more details.

   You should have received a copy of the GNU General Public License along with
   Challenger; see the file COPYING.  If not, see <http://www.gnu.org/licenses/>
 */
/**
 * @file challengerdb/pg_token_add_token.h
 * @brief implementation of the token_add_token function for Postgres
 * @author Christian Grothoff
 */
#ifndef PG_AUTH_ADD_GRANT_H
#define PG_AUTH_ADD_GRANT_H

#include <taler/taler_util.h>
#include <taler/taler_json_lib.h>
#include "challenger_database_plugin.h"


/**
 * Add access @a grant to address under @a nonce.
 *
 * @param cls closure
 * @param nonce validation process to grant access to
 * @param grant grant token that grants access
 * @param grant_expiration for how long should the grant be valid
 * @param address_expiration for how long after validation do we consider addresses to be valid
 * @return transaction status
 */
enum GNUNET_DB_QueryStatus
CH_PG_token_add_token (
  void *cls,
  const struct CHALLENGER_ValidationNonceP *nonce,
  const struct CHALLENGER_AccessTokenP *grant,
  struct GNUNET_TIME_Relative grant_expiration,
  struct GNUNET_TIME_Relative address_expiration);


#endif
