/*
   This file is part of Challenger
   Copyright (C) 2023 Taler Systems SA

   Challenger is free software; you can redistribute it and/or modify it under the
   terms of the GNU General Public License as published by the Free Software
   Foundation; either version 3, or (at your option) any later version.

   Challenger is distributed in the hope that it will be useful, but WITHOUT ANY
   WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR
   A PARTICULAR PURPOSE.  See the GNU General Public License for more details.

   You should have received a copy of the GNU General Public License along with
   Challenger; see the file COPYING.  If not, see <http://www.gnu.org/licenses/>
 */
/**
 * @file challengerdb/pg_challenge_set_address_and_pin.h
 * @brief implementation of the challenge_set_address_and_pin function for Postgres
 * @author Christian Grothoff
 */
#ifndef PG_CHALLENGE_SET_ADDRESS_AND_PIN_H
#define PG_CHALLENGE_SET_ADDRESS_AND_PIN_H

#include <taler/taler_util.h>
#include <taler/taler_json_lib.h>
#include "challenger_database_plugin.h"


/**
 * Set the user-provided address in a validation process. Updates
 * the address and decrements the "addresses left" counter.  If the
 * address did not change, the operation is successful even without
 * the counter change.
 *
 * @param cls
 * @param nonce unique nonce to use to identify the validation
 * @param address the new address to validate
 * @param validation_duration minimum time between transmissions
 * @param[in,out] tan set to the PIN/TAN last send to @a address, input should be random PIN/TAN to use if address did not change
 * @param[out] state set to client's OAuth2 state if available
 * @param[out] last_tx_time set to the last time when we (presumably) send a PIN to @a address, input should be current time to use if the existing value for tx_time is past @a next_tx_time
 * @param[out] pin_transmit set to true if we should transmit the @a last_pin to the @a address
 * @param[out] auth_attempts_left set to number of attempts the user has left on this pin
 * @param[out] client_redirect_uri redirection URI of the client (for reporting failures)
 * @param[out] address_refused set to true if the address was refused (address change attempts exhausted)
 * @param[out] solved set to true if the challenge is already solved
 * @return transaction status:
 *   #GNUNET_DB_STATUS_SUCCESS_ONE_RESULT if the address was changed
 *   #GNUNET_DB_STATUS_SUCCESS_NO_RESULTS if we do not permit further changes to the address (attempts exhausted)
 *   #GNUNET_DB_STATUS_HARD_ERROR on failure
 */
enum GNUNET_DB_QueryStatus
CH_PG_challenge_set_address_and_pin (
  void *cls,
  const struct CHALLENGER_ValidationNonceP *nonce,
  const json_t *address,
  struct GNUNET_TIME_Relative validation_duration,
  uint32_t *tan,
  char **state,
  struct GNUNET_TIME_Absolute *last_tx_time,
  uint32_t *auth_attempts_left,
  bool *pin_transmit,
  char **client_redirect_uri,
  bool *address_refused,
  bool *solved);

#endif
