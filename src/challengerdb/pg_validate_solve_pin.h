/*
   This file is part of Challenger
   Copyright (C) 2023 Taler Systems SA

   Challenger is free software; you can redistribute it and/or modify it under the
   terms of the GNU General Public License as published by the Free Software
   Foundation; either version 3, or (at your option) any later version.

   Challenger is distributed in the hope that it will be useful, but WITHOUT ANY
   WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR
   A PARTICULAR PURPOSE.  See the GNU General Public License for more details.

   You should have received a copy of the GNU General Public License along with
   Challenger; see the file COPYING.  If not, see <http://www.gnu.org/licenses/>
 */
/**
 * @file challengerdb/pg_validate_solve_pin.h
 * @brief implementation of the validate_solve_pin function for Postgres
 * @author Christian Grothoff
 */
#ifndef PG_VALIDATE_SOLVE_PIN_H
#define PG_VALIDATE_SOLVE_PIN_H

#include <taler/taler_util.h>
#include <taler/taler_json_lib.h>
#include "challenger_database_plugin.h"


/**
 * Check PIN entered to validate an address.
 *
 * @param cls
 * @param nonce unique nonce to use to identify the validation
 * @param new_pin the PIN the user entered
 * @param[out] solved set to true if the PIN was correct
 * @param[out] exhausted set to true if the number of attempts to enter the correct PIN has been exhausted
 * @param[out] no_challenge set to true if we never even issued a challenge
 * @param[out] state set to client's OAuth2 state if available
 * @param[out] addr_left set to number of address changes remaining
 * @param[out] auth_attempts_left set to number of authentication attempts remaining
 * @param[out] pin_transmissions_left set to number of times the PIN can still be re-requested
 * @param[out] client_redirect_uri set to OAuth2 client redirect URI
 * @return transaction status:
 *   #GNUNET_DB_STATUS_SUCCESS_ONE_RESULT if the nonce was found
 *   #GNUNET_DB_STATUS_SUCCESS_NO_RESULTS if we do not know the nonce
 *   #GNUNET_DB_STATUS_HARD_ERROR on failure
 */
enum GNUNET_DB_QueryStatus
CH_PG_validate_solve_pin (
  void *cls,
  const struct CHALLENGER_ValidationNonceP *nonce,
  uint32_t new_pin,
  bool *solved,
  bool *exhausted,
  bool *no_challenge,
  char **state,
  uint32_t *addr_left,
  uint32_t *auth_attempts_left,
  uint32_t *pin_transmissions_left,
  char **client_redirect_uri);


#endif
