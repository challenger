/*
   This file is part of Challenger
   Copyright (C) 2023 Taler Systems SA

   Challenger is free software; you can redistribute it and/or modify it under the
   terms of the GNU General Public License as published by the Free Software
   Foundation; either version 3, or (at your option) any later version.

   Challenger is distributed in the hope that it will be useful, but WITHOUT ANY
   WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR
   A PARTICULAR PURPOSE.  See the GNU General Public License for more details.

   You should have received a copy of the GNU General Public License along with
   Challenger; see the file COPYING.  If not, see <http://www.gnu.org/licenses/>
 */
/**
 * @file challengerdb/pg_client_check.h
 * @brief implementation of the client_check function for Postgres
 * @author Christian Grothoff
 */
#ifndef PG_CLIENT_CHECK_H
#define PG_CLIENT_CHECK_H

#include <taler/taler_util.h>
#include <taler/taler_json_lib.h>
#include "challenger_database_plugin.h"


/**
 * Check if a client is in the list of authorized clients. If @a
 * counter_increment is non-zero, the validation counter of the
 * client is incremented by the given value if the client was found.
 *
 * @param cls
 * @param client_id unique row of the client
 * @param client_secret secret of the client
 * @param counter_increment change in validation counter
 * @param[out] client_url set to URL of the client (if any)
 * @return transaction status
 */
enum GNUNET_DB_QueryStatus
CH_PG_client_check (void *cls,
                    uint64_t client_id,
                    const char *client_secret,
                    uint32_t counter_increment,
                    char **client_url);


/**
 * Check if a client is in the list of authorized clients.
 *
 * @param cls
 * @param client_url client redirect URL (if known)
 * @param client_secret secret of the client
 * @param[out] set to client_id ID of the client if found
 * @return transaction status
 */
enum GNUNET_DB_QueryStatus
CH_PG_client_check2 (void *cls,
                     const char *client_url,
                     const char *client_secret,
                     uint64_t *client_id);

#endif
