/*
   This file is part of Challenger
   Copyright (C) 2023 Taler Systems SA

   Challenger is free software; you can redistribute it and/or modify it under the
   terms of the GNU General Public License as published by the Free Software
   Foundation; either version 3, or (at your option) any later version.

   Challenger is distributed in the hope that it will be useful, but WITHOUT ANY
   WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR
   A PARTICULAR PURPOSE.  See the GNU General Public License for more details.

   You should have received a copy of the GNU General Public License along with
   Challenger; see the file COPYING.  If not, see <http://www.gnu.org/licenses/>
 */
/**
 * @file challengerdb/pg_client_check.c
 * @brief Implementation of the client_check function for Postgres
 * @author Christian Grothoff
 */
#include "platform.h"
#include <taler/taler_error_codes.h>
#include <taler/taler_dbevents.h>
#include <taler/taler_pq_lib.h>
#include "pg_client_check.h"
#include "pg_helper.h"


enum GNUNET_DB_QueryStatus
CH_PG_client_check (void *cls,
                    uint64_t client_id,
                    const char *client_secret,
                    uint32_t counter_increment,
                    char **client_redirect_uri)
{
  struct PostgresClosure *pg = cls;
  struct GNUNET_PQ_QueryParam params[] = {
    GNUNET_PQ_query_param_uint64 (&client_id),
    GNUNET_PQ_query_param_string (client_secret),
    GNUNET_PQ_query_param_uint32 (&counter_increment),
    GNUNET_PQ_query_param_end
  };
  struct GNUNET_PQ_ResultSpec rs[] = {
    GNUNET_PQ_result_spec_allow_null (
      GNUNET_PQ_result_spec_string ("uri",
                                    client_redirect_uri),
      NULL),
    GNUNET_PQ_result_spec_end
  };

  *client_redirect_uri = NULL;
  PREPARE (pg,
           "client_check",
           "UPDATE clients SET"
           " validation_counter=validation_counter+CAST($3::INT4 AS INT8)"
           " WHERE client_serial_id=$1"
           "   AND client_secret=$2"
           " RETURNING uri;");
  return GNUNET_PQ_eval_prepared_singleton_select (pg->conn,
                                                   "client_check",
                                                   params,
                                                   rs);
}


enum GNUNET_DB_QueryStatus
CH_PG_client_check2 (void *cls,
                     const char *client_uri,
                     const char *client_secret,
                     uint64_t *client_id)
{
  struct PostgresClosure *pg = cls;
  struct GNUNET_PQ_QueryParam params[] = {
    GNUNET_PQ_query_param_string (client_uri),
    GNUNET_PQ_query_param_string (client_secret),
    GNUNET_PQ_query_param_end
  };
  struct GNUNET_PQ_ResultSpec rs[] = {
    GNUNET_PQ_result_spec_uint64 ("client_serial_id",
                                  client_id),
    GNUNET_PQ_result_spec_end
  };

  PREPARE (pg,
           "client_check2",
           "SELECT client_serial_id"
           " FROM clients"
           " WHERE uri=$1"
           "   AND client_secret=$2;");
  return GNUNET_PQ_eval_prepared_singleton_select (pg->conn,
                                                   "client_check2",
                                                   params,
                                                   rs);
}
