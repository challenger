/*
  This file is part of TALER
  Copyright (C) 2023 Taler Systems SA

  TALER is free software; you can redistribute it and/or modify it under the
  terms of the GNU Affero General Public License as published by the Free Software
  Foundation; either version 3, or (at your option) any later version.

  TALER is distributed in the hope that it will be useful, but WITHOUT ANY
  WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR
  A PARTICULAR PURPOSE.  See the GNU Affero General Public License for more details.

  You should have received a copy of the GNU Affero General Public License along with
  TALER; see the file COPYING.  If not, see <http://www.gnu.org/licenses/>
*/
/**
 * @file challenger-httpd_solve.h
 * @brief functions to handle incoming requests on /solve
 * @author Christian Grothoff
 */
#ifndef CHALLENGER_HTTPD_SOLVE_H
#define CHALLENGER_HTTPD_SOLVE_H

#include <microhttpd.h>


/**
 * Handle a client POSTing a /solve request
 *
 * @param hc context of the connection
 * @param upload_data upload data, if any
 * @param[in,out] upload_data_size remaining data in @a upload_data, to be updated
 * @return MHD result code
 */
MHD_RESULT
CH_handler_solve (struct CH_HandlerContext *hc,
                  const char *upload_data,
                  size_t *upload_data_size);


#endif
