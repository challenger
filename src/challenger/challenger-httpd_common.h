/*
  This file is part of Challenger
  Copyright (C) 2023 Taler Systems SA

  Challenger is free software; you can redistribute it and/or modify it under the
  terms of the GNU Affero General Public License as published by the Free Software
  Foundation; either version 3, or (at your option) any later version.

  Challenger is distributed in the hope that it will be useful, but WITHOUT ANY
  WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR
  A PARTICULAR PURPOSE.  See the GNU Affero General Public License for more details.

  You should have received a copy of the GNU Affero General Public License along with
  Challenger; see the file COPYING.  If not, see <http://www.gnu.org/licenses/>
*/
/**
 * @file challenger-httpd_common.h
 * @brief common helper functions
 * @author Christian Grothoff
 */
#ifndef CHALLENGER_HTTPD_COMMON_H
#define CHALLENGER_HTTPD_COMMON_H

#include "challenger-httpd.h"

/**
 * Extract the client secret from the
 * authorization header of @a connection.
 *
 * @param connection HTTP connection to get client secret from
 * @return NULL if there is no well-formed secret
 */
const char *
CH_get_client_secret (struct MHD_Connection *connection);


/**
 * Return desired output format.
 *
 * @param connection connection to check
 * @return 0 for HTML and 1 for JSON.
 */
int
CH_get_output_type (struct MHD_Connection *connection);


/**
 * Compute code that would grant access to the ``/token``
 * endpoint to obtain an access token for a particular
 * challenge address. NOTE: We may not want
 * to include all of these when hashing...
 *
 * @param nonce nonce of the challenge process
 * @param client_secret secret of the client that should receive access
 * @param client_scope scope of the grant
 * @param address address that access is being granted to
 * @param client_redirect_uri redirect URI of the client
 * @return code that grants access
 */
char *
CH_compute_code (const struct CHALLENGER_ValidationNonceP *nonce,
                 const char *client_secret,
                 const char *client_scope,
                 const json_t *address,
                 const char *client_redirect_uri);


/**
 * Extracts a @a nonce from the given @a code.
 *
 * @param code access code computed via #CH_compute_code()
 * @param[out] nonce set to nonce used in #CH_compute_code()
 * @return #GNUNET_OK on success
 */
enum GNUNET_GenericReturnValue
CH_code_to_nonce (const char *code,
                  struct CHALLENGER_ValidationNonceP *nonce);


/**
 * Send a OAuth 2.0 response indicating an error following
 * section 5.2 of RFC 6749.
 *
 * @param connection the MHD connection to use
 * @param ec error code uniquely identifying the error
 * @param oauth_error error as of the OAuth 2.0 protocol
 * @param http_status HTTP status code to use
 * @param detail additional optional detail about the error
 * @return a MHD result code
 */
MHD_RESULT
CH_reply_with_oauth_error (
  struct MHD_Connection *connection,
  unsigned int http_status,
  const char *oauth_error,
  enum TALER_ErrorCode ec,
  const char *detail);


/**
 * Redirect the client on @a connection to the given
 * @a client_redirect_uri providing the given OAuth2.0
 * error details.
 *
 * @param connection HTTP request to handle
 * @param client_redirect_uri base URI where to redirect
 * @param state client state to pass to server
 * @param oauth_error error status to return (e.g. "invalid_scope")
 * @param oauth_error_description longer description to return, optional, can be NULL
 * @param oauth_error_uri URI with additional information about the error, optional, can be NULL
 * @return MHD response queueing status
 */
MHD_RESULT
TALER_MHD_redirect_with_oauth_status (
  struct MHD_Connection *connection,
  const char *client_redirect_uri,
  const char *state,
  const char *oauth_error,
  const char *oauth_error_description,
  const char *oauth_error_uri);

#endif
