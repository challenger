/*
  This file is part of Challenger
  Copyright (C) 2023 Taler Systems SA

  Challenger is free software; you can redistribute it and/or modify it under the
  terms of the GNU Affero General Public License as published by the Free Software
  Foundation; either version 3, or (at your option) any later version.

  Challenger is distributed in the hope that it will be useful, but WITHOUT ANY
  WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR
  A PARTICULAR PURPOSE.  See the GNU Affero General Public License for more details.

  You should have received a copy of the GNU Affero General Public License along with
  Challenger; see the file COPYING.  If not, see <http://www.gnu.org/licenses/>
*/
/**
 * @file challenger_cm_enums.h
 * @brief enums to handle challenge method
 * @author Bohdan Potuzhnyi
 * @author Vlada Svirsh
 */

#ifndef CHALLENGER_CM_ENUMS_H
#define CHALLENGER_CM_ENUMS_H

#include <stdint.h>

/**
 * Different types of RFC 7636 code_challenges.
 */
enum CHALLENGER_CM
{
  /**
   * No code challenge set.
   */
  CHALLENGER_CM_EMPTY,

  /**
   * Plain mode, challenge is equal to the verifier.
   */
  CHALLENGER_CM_PLAIN,

  /**
   * SHA-256 mode, code_challenge = BASE64URL-ENCODE(SHA256(code_verifier))
   */
  CHALLENGER_CM_S256,

  /**
   * Unknown mode (unsupported input).
   */
  CHALLENGER_CM_UNKNOWN
};


/**
 * Convert a string to the corresponding enum value.
 *
 * @param method_str the string representing the code challenge method
 * @return the corresponding enum value, or CHALLENGER_CM_UNKNOWN if not recognized
 */
enum CHALLENGER_CM
CHALLENGER_cm_from_string (const char *method_str);


/**
 * Convert an int to the corresponding enum value.
 * Returns CHALLENGER_CM_UNKNOWN if the int does not match a valid enum value.
 *
 * @param method_int integer representation of the code challenge method
 * @return the corresponding enum value
 */
enum CHALLENGER_CM
CHALLENGER_cm_from_int (uint32_t method_int);


#endif /* CHALLENGER_CM_ENUMS_H */
