/*
  This file is part of Challenger
  Copyright (C) 2023 Taler Systems SA

  Challenger is free software; you can redistribute it and/or modify it under the
  terms of the GNU Affero General Public License as published by the Free Software
  Foundation; either version 3, or (at your option) any later version.

  Challenger is distributed in the hope that it will be useful, but WITHOUT ANY
  WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR
  A PARTICULAR PURPOSE.  See the GNU Affero General Public License for more details.

  You should have received a copy of the GNU Affero General Public License along with
  Challenger; see the file COPYING.  If not, see <http://www.gnu.org/licenses/>
*/
/**
 * @file challenger-httpd_setup.h
 * @brief functions to handle incoming requests on /setup
 * @author Christian Grothoff
 */
#ifndef CHALLENGER_HTTPD_SETUP_H
#define CHALLENGER_HTTPD_SETUP_H

#include "challenger-httpd.h"


/**
 * Handle request on @a connection for /setup.
 *
 * @param hc context of the connection
 * @param upload_data upload data, if any
 * @param[in,out] upload_data_size remaining data in @a upload_data, to be updated
 * @return MHD result code
 */
MHD_RESULT
CH_handler_setup (struct CH_HandlerContext *hc,
                  const char *upload_data,
                  size_t *upload_data_size);


#endif
