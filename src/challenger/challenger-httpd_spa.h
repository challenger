/*
  This file is part of Challenger
  Copyright (C) 2023 Taler Systems SA

  Challenger is free software; you can redistribute it and/or modify it under the
  terms of the GNU Lesser General Public License as published by the Free Software
  Foundation; either version 3, or (at your option) any later version.

  Challenger is distributed in the hope that it will be useful, but WITHOUT ANY
  WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR
  A PARTICULAR PURPOSE.  See the GNU General Public License for more details.

  You should have received a copy of the GNU General Public License along with
  Challenger; see the file COPYING.GPL.  If not, see <http://www.gnu.org/licenses/>
*/
/**
 * @file challenger-httpd_spa.h
 * @brief logic to preload and serve static files
 * @author Sebastian Marchano
 * @author Christian Grothoff
 */
#ifndef CHALLENGER_HTTPD_SPA_H
#define CHALLENGER_HTTPD_SPA_H
#include <microhttpd.h>
#include "challenger-httpd.h"


/**
 * Return our single-page-app user interface (see contrib/wallet-core/).
 *
 * @param rc context of the handler
 * @param[in,out] args remaining arguments (ignored)
 * @return #MHD_YES on success (reply queued), #MHD_NO on error (close connection)
 */
MHD_RESULT
CH_handler_spa (struct CH_HandlerContext *hc,
                const char *upload_data,
                size_t *upload_data_size);


/**
 * Preload and compress SPA files.
 *
 * @return #GNUNET_OK on success
 */
enum GNUNET_GenericReturnValue
CH_spa_init (void);


/**
 * Generates the response for "/", redirecting the
 * client to the "/webui/" from where we serve the SPA.
 *
 * @param rh request handler
 * @param connection MHD connection
 * @param hc handler context
 * @return MHD result code
 */
MHD_RESULT
CH_spa_redirect (struct CH_HandlerContext *hc,
                 const char *upload_data,
                 size_t *upload_data_size);

#endif

/* end of challenger-httpd_spa.h */
