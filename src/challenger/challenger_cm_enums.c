/*
  This file is part of Challenger
  Copyright (C) 2023 Taler Systems SA

  Challenger is free software; you can redistribute it and/or modify it under the
  terms of the GNU Affero General Public License as published by the Free Software
  Foundation; either version 3, or (at your option) any later version.

  Challenger is distributed in the hope that it will be useful, but WITHOUT ANY
  WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR
  A PARTICULAR PURPOSE.  See the GNU Affero General Public License for more details.

  You should have received a copy of the GNU Affero General Public License along with
  Challenger; see the file COPYING.  If not, see <http://www.gnu.org/licenses/>
*/
/**
 * @file challenger_cm_enums.c
 * @brief enums to handle challenge method
 * @author Bohdan Potuzhnyi
 * @author Vlada Svirsh
 */
#include "challenger_cm_enums.h"
#include <string.h>
#include <stdint.h>


enum CHALLENGER_CM
CHALLENGER_cm_from_string (const char *method_str)
{
  if ( (NULL == method_str) ||
       (0 == strcmp (method_str,
                     "")) )
    return CHALLENGER_CM_EMPTY;
  if (0 == strcmp (method_str,
                   "plain"))
    return CHALLENGER_CM_PLAIN;
  if ( (0 == strcmp (method_str,
                     "S256")) ||
       (0 == strcmp (method_str,
                     "sha256")))
    return CHALLENGER_CM_S256;
  return CHALLENGER_CM_UNKNOWN;
}


enum CHALLENGER_CM
CHALLENGER_cm_from_int (uint32_t method_int)
{
  switch (method_int)
  {
  case 0:
    return CHALLENGER_CM_EMPTY;
  case 1:
    return CHALLENGER_CM_PLAIN;
  case 2:
    return CHALLENGER_CM_S256;
  default:
    /* Invalid or unrecognized value */
    return CHALLENGER_CM_UNKNOWN;
  }
}
