/*
  This file is part of Challenger
  Copyright (C) 2023 Taler Systems SA

  Challenger is free software; you can redistribute it and/or modify it under the
  terms of the GNU Affero General Public License as published by the Free Software
  Foundation; either version 3, or (at your option) any later version.

  Challenger is distributed in the hope that it will be useful, but WITHOUT ANY
  WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR
  A PARTICULAR PURPOSE.  See the GNU Affero General Public License for more details.

  You should have received a copy of the GNU Affero General Public License along with
  Challenger; see the file COPYING.  If not, see <http://www.gnu.org/licenses/>
*/

/**
 * @file challenger-httpd_mhd.h
 * @brief helpers for MHD interaction, used to generate simple responses
 * @author Florian Dold
 * @author Benedikt Mueller
 * @author Christian Grothoff
 */
#ifndef CHALLENGER_HTTPD_MHD_H
#define CHALLENGER_HTTPD_MHD_H
#include <gnunet/gnunet_util_lib.h>
#include <microhttpd.h>
#include "challenger-httpd.h"


/**
 * Function to call to for "/" requests.
 *
 * @param[in,out] hc handler context
 * @param upload_data data that is being uploaded
 * @param[in,out] upload_data_size number of bytes (left) in @a upload_data
 * @return MHD result code
 */
MHD_RESULT
CH_MHD_handler_root (struct CH_HandlerContext *hc,
                     const char *upload_data,
                     size_t *upload_data_size);


/**
 * Function to call to handle the request by sending
 * back a redirect to the AGPL source code.
 *
 * @param hc handler context
 * @param upload_data upload data
 * @param[in,out] upload_data_size number of bytes (left) in @a upload_data
 * @return MHD result code
 */
MHD_RESULT
CH_MHD_handler_agpl_redirect (struct CH_HandlerContext *hc,
                              const char *upload_data,
                              size_t *upload_data_size);


#endif
