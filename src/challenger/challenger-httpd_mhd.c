/*
  This file is part of Challenger
  Copyright (C) 2023 Taler Systems SA

  Challenger is free software; you can redistribute it and/or modify it under the
  terms of the GNU Affero General Public License as published by the Free Software
  Foundation; either version 3, or (at your option) any later version.

  Challenger is distributed in the hope that it will be useful, but WITHOUT ANY
  WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR
  A PARTICULAR PURPOSE.  See the GNU Affero General Public License for more details.

  You should have received a copy of the GNU Affero General Public License along with
  Challenger; see the file COPYING.  If not, see <http://www.gnu.org/licenses/>
*/
/**
 * @file challenger-httpd_mhd.c
 * @brief helpers for MHD interaction
 * @author Christian Grothoff
 */
#include "platform.h"
#include <jansson.h>
#include "challenger-httpd_mhd.h"


MHD_RESULT
CH_MHD_handler_root (struct CH_HandlerContext *hc,
                     const char *upload_data,
                     size_t *upload_data_size)
{
  const char *msg =
    "Hello, I'm challenger. This HTTP server is not for humans.\n";

  (void) upload_data;
  (void) upload_data_size;
  return TALER_MHD_reply_static (hc->connection,
                                 MHD_HTTP_OK,
                                 "text/plain",
                                 msg,
                                 strlen (msg));
}


MHD_RESULT
CH_MHD_handler_agpl_redirect (struct CH_HandlerContext *hc,
                              const char *upload_data,
                              size_t *upload_data_size)
{
  (void) upload_data;
  (void) upload_data_size;
  return TALER_MHD_reply_agpl (hc->connection,
                               "https://git.taler.net/challenger.git");
}


/* end of challenger-httpd_mhd.c */
