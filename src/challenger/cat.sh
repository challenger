#!/bin/bash
# This file is in the public domain.
cat - > "$(echo $1 | jq -r ".filename")"
exit 0
