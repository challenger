/*
  This file is part of Challenger
  Copyright (C) 2023 Taler Systems SA

  Challenger is free software; you can redistribute it and/or modify it under the
  terms of the GNU Lesser General Public License as published by the Free Software
  Foundation; either version 3, or (at your option) any later version.

  Challenger is distributed in the hope that it will be useful, but WITHOUT ANY
  WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR
  A PARTICULAR PURPOSE.  See the GNU General Public License for more details.

  You should have received a copy of the GNU General Public License along with
  Challenger; see the file COPYING.GPL.  If not, see <http://www.gnu.org/licenses/>
*/
/**
 * @file challenger/challenger-httpd_agpl.h
 * @brief headers for /agpl handler
 * @author Christian Grothoff
 */
#ifndef CHALLENGER_HTTPD_AGPL_H
#define CHALLENGER_HTTPD_AGPL_H
#include <microhttpd.h>
#include "challenger-httpd.h"

/**
 * Manages a /agpl call.
 *
 * @param[in,out] hc context of the connection
 * @param upload_data upload data
 * @param[in,out] upload_data_size number of bytes (left) in @a upload_data
 * @return MHD result code
 */
MHD_RESULT
CH_handler_agpl (struct CH_HandlerContext *hc,
                 const char *upload_data,
                 size_t *upload_data_size);

#endif

/* end of challenger-httpd_agpl.h */
