/*
  This file is part of Challenger
  Copyright (C) 2023 Taler Systems SA

  Challenger is free software; you can redistribute it and/or modify it under the
  terms of the GNU Lesser General Public License as published by the Free Software
  Foundation; either version 3, or (at your option) any later version.

  Challenger is distributed in the hope that it will be useful, but WITHOUT ANY
  WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR
  A PARTICULAR PURPOSE.  See the GNU General Public License for more details.

  You should have received a copy of the GNU General Public License along with
  Challenger; see the file COPYING.GPL.  If not, see <http://www.gnu.org/licenses/>
*/
/**
 * @file challenger/challenger-httpd_agpl.c
 * @brief headers for /agpl handler
 * @author Christian Grothoff
 */
#include "platform.h"
#include "challenger-httpd_agpl.h"
#include <taler/taler_mhd_lib.h>


MHD_RESULT
CH_handler_agpl (struct CH_HandlerContext *hc,
                 const char *upload_data,
                 size_t *upload_data_size)
{
  return TALER_MHD_reply_agpl (
    hc->connection,
    "https://git.taler.net/challenger.git/");
}


/* end of challenger-httpd_agpl.c */
