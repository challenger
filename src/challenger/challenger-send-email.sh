#!/bin/sh
# This file is in the public domain.
EMAIL=$(echo "$1" | jq -r .email)
exec mail -s "KYC Challenger" -r noreply "$EMAIL"
