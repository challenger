/*
  This file is part of Challenger
  Copyright (C) 2023 Taler Systems SA

  Challenger is free software; you can redistribute it and/or modify it under the
  terms of the GNU General Public License as published by the Free Software
  Foundation; either version 3, or (at your option) any later version.

  Challenger is distributed in the hope that it will be useful, but WITHOUT ANY
  WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR
  A PARTICULAR PURPOSE.  See the GNU General Public License for more details.

  You should have received a copy of the GNU General Public License along with
  Challenger; see the file COPYING.  If not, see <http://www.gnu.org/licenses/>
*/
/**
 * @file include/challenger_database_lib.h
 * @brief database helper functions for postgres used by challenger
 * @author Christian Grothoff
 */
#ifndef CHALLENGER_DB_LIB_H
#define CHALLENGER_DB_LIB_H

#include <taler/taler_util.h>
#include "challenger_database_plugin.h"

/**
 * Initialize the plugin.
 *
 * @param cfg configuration to use
 * @param skip_preflight true if we should skip the usual
 *   preflight check which assures us that the DB is actually
 *   operational; only challenger-dbinit should use true here.
 * @return NULL on failure
 */
struct CHALLENGER_DatabasePlugin *
CHALLENGER_DB_plugin_load (const struct GNUNET_CONFIGURATION_Handle *cfg,
                           bool skip_preflight);


/**
 * Shutdown the plugin.
 *
 * @param[in] plugin plugin to unload
 */
void
CHALLENGER_DB_plugin_unload (struct CHALLENGER_DatabasePlugin *plugin);


#endif  /* CHALLENGER_DB_LIB_H */

/* end of challenger_database_lib.h */
