/*
  This file is part of CHALLENGER
  Copyright (C) 2023 Taler Systems SA

  CHALLENGER is free software; you can redistribute it and/or modify it under the
  terms of the GNU General Public License as published by the Free Software
  Foundation; either version 3, or (at your option) any later version.

  CHALLENGER is distributed in the hope that it will be useful, but WITHOUT ANY
  WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR
  A PARTICULAR PURPOSE.  See the GNU General Public License for more details.

  You should have received a copy of the GNU General Public License along with
  CHALLENGER; see the file COPYING.  If not, see <http://www.gnu.org/licenses/>
*/
/**
 * @file include/challenger_util.h
 * @brief Interface for common utility functions
 * @author Christian Grothoff
 */
#ifndef CHALLENGER_UTIL_H
#define CHALLENGER_UTIL_H

#include <gnunet/gnunet_util_lib.h>

/**
 * Return project data used by Challenger.
 *
 * @return project data for challenger
 */
const struct GNUNET_OS_ProjectData *
CHALLENGER_project_data (void);


#endif
