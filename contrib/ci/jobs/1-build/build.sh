#!/bin/bash
set -exuo pipefail

./bootstrap
./configure CFLAGS="-ggdb -O0" \
	    --prefix=/usr \
	    --enable-logging=verbose \
	    --disable-doc

make
