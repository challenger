#!/bin/bash
set -exuo pipefail

job_dir=$(dirname "${BASH_SOURCE[0]}")

skip=$(cat <<EOF
ABOUT-NLS
configure
config.guess
configure~
*/debian/upstream/*
*/debian/.debhelper/*
*/contrib/wallet-core/*
*/doc/prebuilt/*
*/doc/doxygen/*
*build-aux*
*.cache/*
*/.git/*
*/contrib/ci/*
depcomp
*libtool*
ltmain.sh
*.log
*/m4/*
*.m4
*.rpath
EOF
);

echo Current directory: `pwd`

codespell -I "${job_dir}"/dictionary.txt -S ${skip//$'\n'/,}
